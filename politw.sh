#!/bin/zsh
# vim:set sw=4 ts=4 sts=4 ft=sh expandtab:

j=1
while read i
do
    print $j
    (( j = $j + 1))
    iceweasel "https://twitter.com/share?text=Bonjour%20$i%0AÊtes-vous pour ou contre le %23PJLRenseignement ?%0APour ou contre la surveillance de masse ? %23PJLSurveillance" 2>/dev/null
    sleep 1
done < politwitts_cleaned.txt
