# Fonction

Le script `politw.sh` lit le fichier `politwitts_cleaned.txt`  et ouvre un onglet dans iceweasel pour tweeter à chaque compte du fichier.

Le fichier `politwitts_cleaned.txt` a été rempli à partir du wiki de la quadrature du net : <https://wiki.laquadrature.net/Politwitts#France>

# Usage : 

Éditez le message à envoyer à vos députés / sénateurs dans `politw.sh` (modifiez le nom de votre navigateur au besoin), puis :

```
./politw.sh
```

Allez boire un café (ça prend un sacré temps d'ouvrir tous ces onglets) puis, sur chaque onglet, cliquez sur le bouton "Tweeter".

Il est possible, probable même, que Twitter vous bloque pour cause de dépassement de limite. Attendez le temps qu'il faut et continuez à tweeter.

# Version HTML

Faites pointer votre `virtualhost` sur le répertoire `html` et mettez en place une tâche `cron` pour être sûr de toujours avoir la dernière version de la liste des députés :

```
34 3 * * * curl http://sous-surveillance.fr/assets/data/deputes_infos.json > html/data/deputes_infos.json
```

# License

        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
                    Version 2, December 2004 

 Copyright (C) 2015 Luc Didry <luc@didry.org>

 Everyone is permitted to copy and distribute verbatim or modified 
 copies of this license document, and changing it is allowed as long 
 as the name is changed. 

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 

  0. You just DO WHAT THE FUCK YOU WANT TO.
